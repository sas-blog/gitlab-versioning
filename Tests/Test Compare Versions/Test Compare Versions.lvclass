﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&gt;+5F.31QU+!!.-6E.$4%*76Q!!%MA!!!3#!!!!)!!!%KA!!!!C!!!!!2V5:8.U)%.P&lt;8"B=G5A6G6S=WFP&lt;H-O&lt;(:D&lt;'&amp;T=Q!!!!!!I#!!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!!$DM35)C`53;L,D0I]13P=!!!!$!!!!"!!!!!!NB[F-9GN8E'Z=I%6]RWFI&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!!!!!!!!!#?+E_0F&lt;\V)O_E]^"CB&amp;8)"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1H95W&lt;HR"[P]F9QJF,#[1X!!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#=!!!!E?*RDY'2A;G#YQ!$%D%$-V-$U!]D_!/)T#(#!3!9'!,%3#;%!!!!!2Q!!!2BYH'.AQ!4`A1")-4)Q-*U#UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"T#,S$!&gt;"+)4[#&lt;QYD&amp;&lt;!"YX3A5!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!&amp;O!!!#X(C=3W"E9-AUND!\!+3:A6C"I9%B/4]FF9M"S'?!A#.-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@\;,#UFSDQM.5SP?`2)5DY!6)M0E)R_&amp;ODZTDDD:A*2R:$&amp;E-!@]$-ZK0])"V)_PX57%R0."1K=R1+HS]U9129EMAV"C7QU$T"9"K?,J$6&amp;A[O]3!4OJMEA+3$:9-`&amp;-0.FA!S1-'*UQ9_&lt;=&gt;/-QP\Q"W$V!^R]'(,,U4Q2:UAMD/%%;*YS[-/G*!&gt;C]09S$#H7DO#?M'_C+,-1QGX]VWX%%$R$\O)!+B-C"5"91K!&amp;%\1%4=97,#&gt;?XL?\N!Y=K'*/9!R1V!$)J8'.:D9'1!"1A4%)*M`@0``X]&lt;I!A46%Q2+A:CHY3S.:$UP)3+/3$:!^),-G%NE.;!MD&gt;$W1V1&gt;Y(%PA,J!CD\$Q-E09$9I$3X!-LG!L)&amp;I'R")0M"F#U':2_!RC9[\?TPYII=*L$U#A$[:X65!!!!!!!/)!'!!!!!"D)Q,D!O-1!!!!!!!!QA!)!!!!!%-D!O-!!!!!!/)!'!!!!!"D)Q,D!O-1!!!!!!!!QA!)!!!!!%-D!O-!!!!!!/)!'!!!!!"D)Q,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!=2C0A(NXX`0\/&gt;`T_X\@]`M2X`0```_-!!!"D!!!!9Q%!!'!#]!"A")9!9!C!Y'!1A'"A))$A9'#"I'"]A;"A@Y-A9(`W)'"``#"A@`QA9(``)'"``?"A@`R!9(`]A'"``9"A0`E!9!@[!'!!@!"A!!A!9!!!!(`````!!!#!0`````````````````````Q!!!!$`]!$Q!0]!$Q!0``]!!!!!``]0]0`Q```Q``````!0````$`!0`Q$`]0`````Q$````Q`Q````$`$`````]!````]0]!$Q!0`Q``````!0`````````````````Q!#)!!!!!!!!!!!!!!!!0]!!C!!!!!!!!!!!!!!!!$`!!)A!!!-,-!!!!!!!!!!`Q!!!!!!TG:G,-!!!!!!!0]!!!!!$#:G:G:CT!!!!!$`!!!!!-*G:G:G:G&lt;N!!!!`Q!!!!!G:G:G:G:G9A!!!0]!!!!/:G:G:G:G:O9!!!$`!!!!ZG:G:G:G:GZG!!!!`Q!!!/\O:G:G:G&lt;G:A!!!0]!!!$G:G\G:G:O:G9!!!$`!!!!*G:G:OZGZG:G!!!!`Q!!!#:G:G:G9G:G:A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:GQ!!!$`!!!!*G:G:G:O:G&lt;!!!!!`Q!!!':G:G:G&lt;G:C!!!!!0]!!!$.ZG:G:GZG)!!!!!$`!!!!!!T?:G:O9M!!!!!!`Q!!!!!!!-XG&lt;GQ!!!!!!0]!!!!!!!!!$.,!!!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````S1E*#1E*#1E````!!!!`Q!!!0``!!!!`Q!!!0``````*#1E*#1E*#4`````!0``!0```Q$``````Q$```````````]E*0````````]!``]!!0```Q!!````!0```````````S1E`````````Q$``Q$```````]!``]!````````````*#4`````````!0``!!!!`Q!!!0```Q$```````````]E*0```````````````````````````````````Q!!!.04!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!U^-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!$4UQ!!!!!!!0;D?@A!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!$WRXCCH-;&gt;6@9!!!!!!!!!!!!!!0``!!!!!!!!!!!!^K.Y?+*Y?(BYRK.Z+Q!!!!!!!!!!``]!!!!!!!!!!0;D?(BYIHBY?(BY?(C=RY!!!!!!!!$``Q!!!!!!!!!!IXBY?(CC?(BY?(BY?(C=TQ!!!!!!!0``!!!!!!!!!+2Y?(BY?+*Y?(BY?(BYH-@'!!!!!!!!``]!!!!!!!#FH(BY?(BYIHBY?(BY?*T(?-9!!!!!!!$``Q!!!!!!!-@(R]?=?(CC?(BY?(BYRZRYRA!!!!!!!0``!!!!!!!!RZS=H-&lt;(R]:Y?(BY?-?=?(D'!!!!!!!!``]!!!!!!!#DH*S=H*S=RM@(RHD(H(BY?-9!!!!!!!$``Q!!!!!!!+/=H*S=H++=H*T'TJRY?(BYRA!!!!!!!0``!!!!!!!!IZS=H*SCH*S=H*T(H(BY?(CD!!!!!!!!``]!!!!!!!#DH*S=IJS=H*S=H-?CIK*Y?+-!!!!!!!$``Q!!!!!!!+/=H++=H*S=H*S=RZRYIK+CIQ!!!!!!!0``!!!!!!!!IZSCH*S=H*S=H*T(H(BY?*RZ!!!!!!!!``]!!!!!!!#DIJS=H*S=H*S=H-?=?(C=?1!!!!!!!!$``Q!!!!!!!++=H*S=H*S=H*S=RZRYH+-!!!!!!!!!!0``!!!!!!!!_)$(IJS=H*S=H*T(?(CD!!!!!!!!!!!!``]!!!!!!!!!!!$YA-@'H*S=H-&gt;YI`9!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!0B[R];=R]9L!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!+XKL+Q!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!!"!!!!!!!!!.\!!!)V(C=L69\4&amp;.2'0Z0+?9555["]D$50DR&amp;BD9KCCA7)8)&amp;?;O)!1?UM3A99AU0QQ4,F114*B)'%S946A='&amp;Q:D'A?\-$G)3=0!TE)UE&gt;PL@_[FP&lt;?0N"LM]+&gt;JPO^`@&gt;`Z5Q#[T7IM#6B1A,!$`.+H1%EY2A#C!1J(H`IF9-0E.Z$+7K,!)"VG/Z9%K6/A."SLJR?F:&gt;A`1J-G3SP&lt;2?A*6IP*3B1I#]?K\4U]TPC(/LZ=H-RK"S&gt;&lt;)1H,#(@^J#NS"!O#X##C05!31#30V3J\\Y:?DMN=`'I,U&amp;INJ5U"*M7=UTR_(D.C[=^;3IO@=#R^F")Q:1.M&lt;GY;*,N/KN@;O)9=QL6W`8EYZ=C:Y8'`RCH2/&amp;CH+6F(=C672?_#F%GNE')-K=C&lt;4[['*WN*,K5^*W^#A5I?PU6L[7\Y?`'J?`*()%#CD[H[68UD_09"I9+',5=B@#W%$?(X)15OS4(,%\#+7B8!9!T?;T*9ET*=2BF)JS;$46JG9/CQ7E!(S2LV/DOGZG:GR[@&gt;E7@OJV/BG2HXK_H*V[(:=8=Y."P3&amp;7)_P4&gt;E!7]A/)40,.I6+7:L&amp;AM2^87`/-!+8&gt;"O&amp;C!#'RM&lt;O"/-"L5*K&gt;5]HO,:M6IP3#E2QM9S230'-K`C-HWDY(M%UK](T]6;@3W7&gt;$-X'W;_DG;WM]TGUMR]Y`_9O=#KAGC\B1R`1R!;I4_06VNVEO&amp;PR!.U1$!0ZS:S&amp;MX_2EY1/@W&amp;`&gt;W7Z7`"T@$XWNJ;JL`&lt;5Z,I,L=3ILN=XF90V50B^4VV$SZ!G]HLA[A46F&amp;`K0NQ'PTJGH1+:X&gt;JGDA-:R?D+,[#TJ;^!Z(=IFD32#F6I!N.W-DD$\5K:6I6*ZS$"GD%1=`W9GI2\3\^U:&amp;%E4PKUP&gt;S%MJENQ&lt;RC-C]2\N^1&lt;UC@49UL!IIRM,1YW?VO9Q:\_#-64TO3,["'FRD!*JRQ+WN,2Q19[Y"(&lt;A$NQ&lt;RC*D+0ZH&gt;CAY6PP#)7"B[`+TG!&lt;PV)T2K%F(-C-YVOTT8D'&gt;Q$7Y.YB%R65,+\C9H6,S`PU0_?V,TB$X[B#/G#3PUNZEV)5GPY%*)LG9GUELA%_YV8&gt;8\.PWK&amp;O&amp;682I41C"WQ(2S[-[8O&gt;4`#$L%*45@A.X'%IM+/+3/]E_M7T2*O_FNNIY8'I^V&amp;6WH"^&amp;PK8];U&lt;Y55^['NW3?PX02GD]P-F%`!!!!!!1!!!!J!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!"!!!!!!!!!"F!!!!&gt;8C=9W"A+"3190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT```^8+7,E_(LE'FT2%2]Y5W770)=%!'5)':I!!!!!!!!%!!!!"Q!!!H1!!!!)!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!%AA!)!!!!!!!1!)!$$`````!!%!!!!!!#Q!!!!#!!J!)12/&lt;WZF!!!;1&amp;!!!1!!%&amp;2F&lt;8"M982F6'6T&gt;%.B=W5!!!%!!1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!J)!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!WI=Y&amp;1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:)!#!!!!!!!%!"1!(!!!"!!$;BTA6!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!%AA!)!!!!!!!1!)!$$`````!!%!!!!!!#Q!!!!#!!J!)12/&lt;WZF!!!;1&amp;!!!1!!%&amp;2F&lt;8"M982F6'6T&gt;%.B=W5!!!%!!1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'3!!A!!!!!!"!!5!!Q!!!1!!!!!!!1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!!V)!#!!!!!!!)!#E!B"%ZP&lt;G5!!"J!5!!"!!!16'6N='RB&gt;'65:8.U1W&amp;T:1!!!1!"!!!!!!!!!!!54EEO4&amp;9O17RM,F.P&gt;8*D:5^O&lt;(E!!!!6)!#!!!!!!!%!"!!B!!%!!!%!!!!!!!!!"!!$!!M!!!!%!!!!4Q!!!#A!!!!#!!!%!!!!!#I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!`A!!!:&amp;YH)W0-5`$-"#&amp;P]3B45-,"6IEBELOQM$!QN)R5C6'6#(%4*1YK#D56?R7D'T]2XY&amp;`!/O;2%$$/D*Z\PXTO`/Q#F*/IZO\-,!%Z`P&lt;Z-B%)\OD0.[;J_877XUP;H&gt;X#\=:&lt;8/K]SZM\`6X&amp;&gt;=J$-#'%WLF@/GVL&lt;5T2/^L/@LT"N&gt;:$ZD4U"#!=%(I22K6X*&amp;2#&gt;NI]LKE4B^6@&lt;7&lt;QFD3`LCLGD2*F;LIJ4?-&gt;VG9K,]3S%?-\(LM-`Z0`=)E&gt;$;"/G/?:!6)K\&amp;2H0!I=A&lt;^)8\2LD$&lt;_:(/7KQ`6$!M72ZYRP)A:\=3A:W*4NB)-S!I?1^_!*[S5#-!!!!!!#*!!%!!A!$!!9!!!"I!!]%!!!!!!]!W1$5!!!!=1!0"!!!!!!0!.E!V!!!!(I!$Q1!!!!!$Q$:!.1!!!#$A!#%!)!!!!]!W1$5!!!!B9!!B!#!!!!0!.E!V!!!!)?!!)1!A!!!$Q$:!.1)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A%Q!4%!!!"35V*$$1I!!UR71U.-1F:8!!!3S!!!"))!!!!A!!!3K!!!!!!!!!!!!!!!)!!!!$1!!!2I!!!!(ER*1EY!!!!!!!!"?%R75V)!!!!!!!!"D&amp;*55U=!!!!!!!!"I%.$5V1!!!!!!!!"N%R*&gt;GE!!!!!!!!"S%.04F!!!!!!!!!"X&amp;2./$!!!!!!!!!"]%2'2&amp;-!!!!!!!!#"%R*:(-!!!!!!!!#'&amp;:*1U1!!!!!!!!#,(:F=H-!!!!%!!!#1&amp;.$5V)!!!!!!!!#J%&gt;$5&amp;)!!!!!!!!#O%F$4UY!!!!!!!!#T'FD&lt;$1!!!!!!!!#Y'FD&lt;$A!!!!!!!!#^%R*:H!!!!!!!!!$#%:128A!!!!!!!!$(%:13')!!!!!!!!$-%:15U5!!!!!!!!$2&amp;:12&amp;!!!!!!!!!$7%R*9G1!!!!!!!!$&lt;%*%28A!!!!!!!!$A%*%3')!!!!!!!!$F%*%5U5!!!!!!!!$K&amp;:*6&amp;-!!!!!!!!$P%253&amp;!!!!!!!!!$U%V6351!!!!!!!!$Z%B*5V1!!!!!!!!$_&amp;:$6&amp;!!!!!!!!!%$%:515)!!!!!!!!%)!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!/!!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!%5!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"3!!!!!!!!!!!`````Q!!!!!!!!'5!!!!!!!!!!$`````!!!!!!!!!;1!!!!!!!!!"0````]!!!!!!!!$'!!!!!!!!!!(`````Q!!!!!!!!-M!!!!!!!!!!D`````!!!!!!!!!TQ!!!!!!!!!#@````]!!!!!!!!$5!!!!!!!!!!+`````Q!!!!!!!!.A!!!!!!!!!!$`````!!!!!!!!!X1!!!!!!!!!!0````]!!!!!!!!$D!!!!!!!!!!!`````Q!!!!!!!!/A!!!!!!!!!!$`````!!!!!!!!"#1!!!!!!!!!!0````]!!!!!!!!'+!!!!!!!!!!!`````Q!!!!!!!!IM!!!!!!!!!!$`````!!!!!!!!#DQ!!!!!!!!!!0````]!!!!!!!!+2!!!!!!!!!!!`````Q!!!!!!!!X%!!!!!!!!!!$`````!!!!!!!!$=Q!!!!!!!!!!0````]!!!!!!!!.V!!!!!!!!!!!`````Q!!!!!!!!XE!!!!!!!!!!$`````!!!!!!!!$?Q!!!!!!!!!!0````]!!!!!!!!/7!!!!!!!!!!!`````Q!!!!!!!!ZA!!!!!!!!!!$`````!!!!!!!!%.A!!!!!!!!!!0````]!!!!!!!!1Y!!!!!!!!!!!`````Q!!!!!!!"$I!!!!!!!!!!$`````!!!!!!!!%21!!!!!!!!!A0````]!!!!!!!!3'!!!!!!:6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2V5:8.U)%.P&lt;8"B=G5A6G6S=WFP&lt;H-O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!"!!"!!!!!!!!!1!!!!)!#E!B"%ZP&lt;G5!!!A!5!!"!!!!!1!"!!!!!!!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!ABA!)!!!!!!!!!!!!!!1!!!!!!!1%!!!!"!&amp;Y!]&gt;J;:)-!!!!#'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=R25:7VQ&lt;'&amp;U:62F=X2$98.F,G.U&lt;!!I1&amp;!!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!!!!!!!!!!!!!%16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!'1#!!!!!!!!!!!!!!!!"!!!!!!!#!1!!!!)!#E!B"%ZP&lt;G5!!'!!]&gt;K(/"5!!!!#'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=R25:7VQ&lt;'&amp;U:62F=X2$98.F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(`````!!!!!!%16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!'1#!!!!!!!!!!!!!!!!"!!!!!!!!!!!!!!)!#E!B"%ZP&lt;G5!!'!!]&gt;K(/"5!!!!#'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=R25:7VQ&lt;'&amp;U:62F=X2$98.F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_!!!!!!%16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!)!#!!!!!!!!!!!!!!!%!!!!96'6N='RB&gt;'65:8.U1W&amp;T:3ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Parent Libraries" Type="Parent Libraries">
		<Item Name="TestCase.lvclass" Type="Parent" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/VI Tester/TestCase.llb/TestCase.lvclass"/>
	</Item>
	<Item Name="Test Compare Versions.ctl" Type="Class Private Data" URL="Test Compare Versions.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1352671760</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!VS:7:F=G6O9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!$(*F:G6S:7ZD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!*!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1084236288</Property>
	</Item>
	<Item Name="testExample.vit" Type="VI" URL="../testExample.vit">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1084236288</Property>
	</Item>
	<Item Name="Test major version newer.vi" Type="VI" URL="../Test major version newer.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Test major version older.vi" Type="VI" URL="../Test major version older.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074274832</Property>
	</Item>
	<Item Name="Test same major but minor older.vi" Type="VI" URL="../Test same major but minor older.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test same major but minor newer.vi" Type="VI" URL="../Test same major but minor newer.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test same major minor but fix older.vi" Type="VI" URL="../Test same major minor but fix older.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test same major minor but fix newer.vi" Type="VI" URL="../Test same major minor but fix newer.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test curiosity.vi" Type="VI" URL="../Test curiosity.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test curiosity make sure only compare fix if both major and minor ae equal.vi" Type="VI" URL="../Test curiosity make sure only compare fix if both major and minor ae equal.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test same major minor fix but build newer.vi" Type="VI" URL="../Test same major minor fix but build newer.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972416</Property>
	</Item>
	<Item Name="Test same major minor fix but build older.vi" Type="VI" URL="../Test same major minor fix but build older.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074266624</Property>
	</Item>
	<Item Name="Test same.vi" Type="VI" URL="../Test same.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%U!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!@(62F=X1A1W^N='&amp;S:3"7:8*T;7^O=SZM&gt;G.M98.T!!R5:8.U1W&amp;T:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"]&gt;6'6T&gt;#"$&lt;WVQ98*F)&amp;:F=H.J&lt;WZT,GRW9WRB=X-!#V2F=X2$98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
	</Item>
</LVClass>
