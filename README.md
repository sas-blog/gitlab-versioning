#### Assumptions
1. Assumes that no older versions are maintained. ie. No maintenance releases. As in making a 1.x release whil 2.x is the current version. This breaks the scheme, because it relies on the latest release (timewise) instead of sorting through them all.
2. Assumes first release link is to an NI or VIPM package and that it includes the version (following the default naming convention) and that the text of the link is the same as the full package name. This is where it pulls the full version number from, not the tag.
3. Assumes that package file is the only thing that needs to be downloaded.
